import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import {AngularFireModule} from 'angularfire2';

import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductFormComponent } from './product-form/product-form.component';

export const firebaseConfig = {
    apiKey: "AIzaSyBL240hjddLykuOwPSwKTbFcJvpavwIg-w",
    authDomain: "pre-test-286bc.firebaseapp.com",
    databaseURL: "https://pre-test-286bc.firebaseio.com",
    storageBucket: "pre-test-286bc.appspot.com",
    messagingSenderId: "17974141320"
}

const appRoutes:Routes =[
  {path:'users',component:UsersComponent},
  {path:'posts',component:PostsComponent},

  {path:'products',component:ProductsComponent},

  {path:'',component:UsersComponent},
  {path:'**',component:PageNotFoundComponent},

] 

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    ProductComponent,
    ProductsComponent,
    ProductFormComponent,
    

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService, ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
