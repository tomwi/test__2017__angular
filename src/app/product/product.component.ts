import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Product} from './product';

@Component({
  selector: '[app-product]',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['product']
})
export class ProductComponent implements OnInit {
  product: Product;
  constructor() { }

  ngOnInit() {
  }

}
