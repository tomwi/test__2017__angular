import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class ProductsComponent implements OnInit {
  products;
  //currentProduct;
  isLoading = true;

  constructor(private _productService: ProductsService) {
  //  this.products = this._productsService.getProducts();
  }

  // ngOnInit() {
  //    this._productService.getProducts().subscribe(productData => {this.products=productData});  
  // }
  addProduct(product){//roni class
    console.log("p com");
    this._productService.addProduct(product);
  }
  ngOnInit() {
        this._productService.getProducts()
			    .subscribe(products => {this.products = products;
                               this.isLoading = false;
                               console.log(products)});
  }
}
