import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Product} from '../product/product';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})


export class ProductFormComponent implements OnInit {
  @Output() productAddedEvent = new EventEmitter<Product>();
  product:Product = {pid:'',pname:'',cost:'',categoryId:''};


  constructor() { }

  onSubmit(form:NgForm){
    console.log(form)
    this.productAddedEvent.emit(this.product);
    this.product = {
      pid:'',
      pname:'',
      cost:'',
      categoryId:''
    }
  }

  ngOnInit() {
  }

}