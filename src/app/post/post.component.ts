import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post';   

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post','currentPost']

})
export class PostComponent implements OnInit {
//left post : השמה , this is the attribute 
// right post : this is the class name and defines the types : string,integer of the hole class, an object of attributes.
post:Post;
isEdit:Boolean = false;
editButtonText = "Edit";
currentPost;

@Output() deleteEvent = new EventEmitter<Post>(); // passing data from son to father using output. 

sendDelete(){
  this.deleteEvent.emit(this.post);
}
toggleEdit(){
 this.isEdit = !this.isEdit;
 this.currentPost = this.post.body
 this.editButtonText ? this.editButtonText = "Save" : this.editButtonText = "Edit"
}
resetPost(){
   console.log(this.currentPost)
   this.currentPost;
}
  constructor() { }

  ngOnInit() {

  }

}
